const mongoose = require("mongoose");

const FoodSchema = new mongoose.Schema({
  foodItem: {
    type: String,
    required: true,
  },
  daysSinceIAte: {
    type: Number,
    required: true,
  },
});

const Food = new mongoose.model("food", FoodSchema);

module.exports = Food;
