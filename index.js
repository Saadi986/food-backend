const express = require("express");
const app = express();
const mongoose = require("mongoose");
const foodModel = require("./models/Food");
const cors = require("cors");

app.use(express.json());
app.use(cors());

app.listen(3001, () => {
  console.log("server running on port 3001...");
});

mongoose.connect(
  "mongodb+srv://mongo8113:mongo8113@cluster0.ax8ri.mongodb.net/food?retryWrites=true&w=majority",
  { useNewUrlParser: true }
);

app.post("/insert", async (req, res) => {
  const foodItem = req.body.food;
  const days = req.body.days;

  const food = new foodModel({ foodItem: foodItem, daysSinceIAte: days });

  try {
    await food.save();
    res.send("Data Inserted");
  } catch (err) {
    console.log(err);
  }
});

app.get("/read", async (req, res) => {
  foodModel.find({}, (err, result) => {
    if (err) {
      res.send(err);
    }

    res.send(result);
  });
});

app.put("/update", async (req, res) => {
  const newFoodName = req.body.newFoodName;
  const id = req.body.id;
  try {
    await foodModel.findById(id, (err, updatedFood) => {
      updatedFood.foodItem = newFoodName;
      updatedFood.save();
      res.send("Data Updated");
    });
  } catch (err) {
    console.log(err);
  }
});

app.delete("/delete/:id", async (req, res) => {
  const id = req.params.id;
  await foodModel.findByIdAndRemove(id).exec();
  res.send("Data Deleted");
});
